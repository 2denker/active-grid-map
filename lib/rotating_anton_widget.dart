import 'dart:math';

import 'package:flutter/material.dart';

class RotatingAntonWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RotationAntonWidgetState();
}

class _RotationAntonWidgetState extends State<RotatingAntonWidget>
    with SingleTickerProviderStateMixin {
  late final AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 800))
          ..repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return Transform.rotate(
          angle: _animationController.value * 2 * pi,
          child: child,
        );
      },
      child: Image.asset("assets/anton.png"),
    );
  }
}
