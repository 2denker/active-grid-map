import 'package:active_grid_map_pins/pin.dart';
import 'package:active_grid_map_pins/rotating_anton_widget.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapWidget extends StatelessWidget {
  final List<Pin> pins;

  MapWidget({Key? key, required this.pins}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final markers = pins.map((pin) {
      return Marker(
          markerId: MarkerId(toString()),
          position: pin.latLng,
          // infoWindow: InfoWindow(title: pin.name),
          onTap: () {
            showDialog(
                context: context,
                builder: (context) {
                  return Padding(
                    padding: const EdgeInsets.all(8),
                    child: Stack(
                      children: [
                        // RotatingAntonWidget(),
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white),
                          padding: EdgeInsets.fromLTRB(20, 50, 20, 20),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(pin.name,
                                  style: TextStyle(fontSize: 24),
                                  textAlign: TextAlign.center),
                              Image.network(pin.imgUrl),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                });
          });
    }).toSet();

    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: pins.first.latLng,
        zoom: 14.4,
      ),
      markers: markers,
    );
  }
}
