import 'package:active_grid_grid_builder/active_grid_grid_builder.dart';
import 'package:active_grid_map_pins/map_widget.dart';
import 'package:active_grid_map_pins/pin.dart';
import 'package:flutter/material.dart';

void main() async {
  await enableWebAuth();
  runApp(
    ActiveGrid(
      options: ActiveGridOptions(
        environment: ActiveGridEnvironment.alpha,
        authenticationOptions: ActiveGridAuthenticationOptions(
          autoAuthenticate: true,
        ),
      ),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ActiveGrid Map',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'ActiveGrid Map Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ActiveGridGridBuilderState> _builderKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ActiveGrid Map'),
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              _builderKey.currentState?.reload();
            },
          )
        ],
      ),
      body: ActiveGridGridBuilder(
          key: _builderKey,
          user: '608938391e3ecf31fc7d6679',
          space: '6089406a1e3ecfd59d7d6686',
          grid: '608940721e3ecfd59d7d6688',
          builder: (context, snapshot) {
            List<Pin> pins = snapshot.data?.rows.map((row) {
                  return Pin.parseRow(row: row);
                }).toList() ??
                [];

            return MapWidget(pins: pins);
          }),
    );
  }
}
