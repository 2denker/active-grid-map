import 'package:active_grid_grid_builder/active_grid_grid_builder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Pin {
  double lat;
  double lon;
  String name;
  String imgUrl;
  String iconUrl;

  Pin(
      {required this.lat,
      required this.lon,
      required this.name,
      this.imgUrl = "",
      this.iconUrl = ""});

  Pin.parseRow({required GridRow row})
      : this.name = row.entries[0].data.value ?? "",
        this.lat = double.parse(row.entries[1].data.value ?? "0"),
        this.lon = double.parse(row.entries[2].data.value ?? "0"),
        this.imgUrl = row.entries[3].data.value ?? "",
        this.iconUrl = row.entries[4].data.value ?? "";

  @override
  String toString() {
    return name;
  }

  LatLng get latLng => LatLng(lat, lon);
}
